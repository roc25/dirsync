#include "DirSync.h"
#include <QFileInfo>
#include <QtListExtensions>
#include <HtStandardModel>

static bool greaterByPath(const FileDescriptor& left, const FileDescriptor& right)
{
   return left.relFilePath > right.relFilePath;
}

static bool lessBySize(const FileDescriptor& left, const FileDescriptor& right)
{
   return left.size < right.size;
}

class SizeGroup
{
public:
   int size = 0;
   QList<FileDescriptor> fileDescriptors;
};

static QList<SizeGroup> groupBySize(const QList<FileDescriptor>& fileDescriptors)
{
   QList<SizeGroup> result;

   SizeGroup currentGroup;

   for (auto&& fileDescriptor : fileDescriptors)
   {
      if (fileDescriptor.size != currentGroup.size)
      {
         if (!currentGroup.fileDescriptors.isEmpty())
         {
            result.append(currentGroup);
            currentGroup.fileDescriptors.clear();
         }
         currentGroup.size = fileDescriptor.size;
      }
      currentGroup.fileDescriptors.append(fileDescriptor);
   }
   if (!currentGroup.fileDescriptors.isEmpty())
   {
      result.append(currentGroup);
   }

   return result;
}

static void addNode(HtNode* parent, const QStringList& filePath, int filePathIndex, const QStringList& rightFilePath, const FileDescriptor& leftFile, const FileDescriptor& rightFile, int status)
{
   if (filePathIndex + 1 < filePath.count())
   {
      // Insert / update Folder

      auto node = parent->binaryFindOrInsert(0, filePath.at(filePathIndex), Qt::UserRole);

      if (node->attribute(0).toString().isEmpty() && !leftFile.relFilePath.isEmpty())
      {
         node->setAttribute(0, QText(filePath.at(filePathIndex)));
      }

      auto remainingRightFilePath = rightFilePath;

      if (!rightFile.relFilePath.isEmpty())
      {
         QString rightFolderName;

         if (remainingRightFilePath.count() > 1 && remainingRightFilePath.first() == filePath.at(filePathIndex))
         {
            rightFolderName = remainingRightFilePath.first();
            remainingRightFilePath.removeFirst();
         }
         else
         {
            rightFolderName = remainingRightFilePath.mid(0, remainingRightFilePath.length() - 1).join("/");
            remainingRightFilePath.prepend("..");
         }

         if (node->attribute(6).toString().isEmpty())
         {
            node->setAttribute(6, rightFolderName);
         }
         else if (rightFolderName != node->attribute(6).toString())
         {
            node->setAttribute(6, "/\\/\\/\\");
         }
      }

      addNode(node, filePath, filePathIndex + 1, remainingRightFilePath, leftFile, rightFile, status);
   }
   else
   {
      // Insert leaf

      auto node = parent->insertChild(parent->upperBound(0, filePath.at(filePathIndex), Qt::UserRole));
      node->setAttribute(0, filePath.at(filePathIndex), Qt::UserRole);

      if (!leftFile.relFilePath.isEmpty())
      {
         node->setAttribute(0, QText(filePath.at(filePathIndex)));
         node->setAttribute(1, leftFile.size);
         node->setAttribute(1, leftFile.filePath, Qt::UserRole);
         node->setAttribute(2, leftFile.lastModified);
         node->setAttribute(2, leftFile.relFilePath.join("/"), Qt::UserRole);
      }
      if (!rightFile.relFilePath.isEmpty())
      {
         node->setAttribute(4, rightFile.lastModified);
         node->setAttribute(4, rightFile.relFilePath.join("/"), Qt::UserRole);
         node->setAttribute(5, rightFile.size);
         node->setAttribute(5, rightFile.filePath, Qt::UserRole);
         node->setAttribute(6, rightFilePath.join("/"));
      }

      node->setAttribute(3, status, Qt::UserRole);

      if (status != (int)Status::Single)
      {
         if ((status & (int)Status::EqualContent) == 0)
         {
            node->setAttribute(1, Qt::red, Qt::TextColorRole);
            node->setAttribute(5, Qt::red, Qt::TextColorRole);
         }

         if ((status & (int)Status::EqualTime) == 0)
         {
            node->setAttribute(2, Qt::red, Qt::TextColorRole);
            node->setAttribute(4, Qt::red, Qt::TextColorRole);
         }
      }
   }
}

static int compareByContent(const QString& leftFilePath, const QString& rightFilePath)
{
   auto cmp = 0;

   QFile leftFile(leftFilePath);
   QFile rightFile(rightFilePath);

   if (!leftFile.open(QIODevice::ReadOnly))
   {
      cmp = -1;
   }
   if (!rightFile.open(QIODevice::ReadOnly))
   {
      leftFile.close();
      cmp++;
      return cmp;
   }

   qint64 leftData;
   qint64 rightData;

   while (cmp == 0 && !leftFile.atEnd() && !rightFile.atEnd())
   {
      leftFile.read((char*)&leftData, sizeof(leftData));
      rightFile.read((char*)&rightData, sizeof(rightData));

      if (leftData < rightData)
         cmp = -1;
      else if (leftData > rightData)
         cmp = 1;
   }

   leftFile.close();
   rightFile.close();

   return cmp;
}

void parse(const QDir &folder, QList<FileDescriptor>& fileList, QList<QStringList>& emptyFolderList, const QStringList& folderPath) // Recursive function!
{
   QFileInfoList fileInfoList = folder.entryInfoList(QDir::Dirs | QDir::Files | QDir::NoSymLinks | QDir::NoDotAndDotDot);

   for (auto&& fileInfo : fileInfoList)
   {
      auto relFilePath = folderPath;

      relFilePath.append(fileInfo.fileName());

      if (fileInfo.isDir())
      {
         auto fileCount = fileList.length();

         parse(fileInfo.filePath(), fileList, emptyFolderList, relFilePath);

         if (fileList.length() == fileCount)
         {
            emptyFolderList += relFilePath;
         }
      }
      else
      {
         FileDescriptor descriptor;

         descriptor.relFilePath = relFilePath;
         descriptor.filePath = fileInfo.canonicalFilePath();
         descriptor.size = fileInfo.size();
         descriptor.lastModified = QDateTimeEx(fileInfo.lastModified().toUTC(), DateTimePart::Second);

         fileList.append(descriptor);
      }
   }
}

HtItemModel* createSyncModel(QList<FileDescriptor> leftList, QList<FileDescriptor> rightList)
{
   auto model = new HtStandardModel;

   // Match by file name and equal content

   std::reverse(leftList.begin(), leftList.end());
   std::reverse(rightList.begin(), rightList.end());
   std::sort(leftList.begin(), leftList.end(), greaterByPath);
   std::sort(rightList.begin(), rightList.end(), greaterByPath);

   auto l = leftList.count();
   auto r = rightList.count();

   while (true)
   {
      while (r && (l == 0 || rightList.at(r - 1).relFilePath < leftList.at(l - 1).relFilePath))
      {
         r--;
      }

      if (l == 0)
      {
         break;
      }

      l--;

      if (r && leftList.at(l).relFilePath == rightList.at(r - 1).relFilePath)
      {
         r--;

         int status = (int)Status::EqualName;

         if (leftList.at(l).lastModified == rightList.at(r).lastModified)
         {
            status |= (int)Status::EqualTime | (int)Status::EqualContent;
         }
         else if (compareByContent(leftList.at(l).filePath, rightList.at(r).filePath) == 0)
         {
            status |= (int)Status::EqualContent;
         }

         if (status != (int)Status::EqualName)
         {
            addNode(model->rootNode(), leftList.at(l).relFilePath, 0, rightList.at(r).relFilePath, leftList.at(l), rightList.at(r), status);
            leftList.removeAt(l);
            rightList.removeAt(r);
         }
      }
   }

   // Match by equal content

   std::sort(leftList.begin(), leftList.end(), lessBySize);
   std::sort(rightList.begin(), rightList.end(), lessBySize);

   auto leftSizes = groupBySize(leftList);
   auto rightSizes = groupBySize(rightList);

   l = leftSizes.count();
   r = rightSizes.count();

   while (true)
   {
      while (r && (l == 0 || rightSizes.at(r - 1).size > leftSizes.at(l - 1).size))
      {
         r--;
      }

      if (l == 0)
      {
         break;
      }

      l--;

      if (r && leftSizes.at(l).size == rightSizes.at(r - 1).size)
      {
         r--;

         for (auto i = leftSizes.at(l).fileDescriptors.count(); i--; )
         {
            for (auto j = rightSizes.at(r).fileDescriptors.count(); j--; )
            {
               auto leftFileDescriptor = leftSizes.at(l).fileDescriptors.at(i);
               auto rightFileDescriptor = rightSizes.at(r).fileDescriptors.at(j);

               if (leftFileDescriptor.lastModified == rightFileDescriptor.lastModified)
               {
                  addNode(model->rootNode(), leftFileDescriptor.relFilePath, 0, rightFileDescriptor.relFilePath, leftFileDescriptor, rightFileDescriptor, (int)Status::EqualTime | (int)Status::EqualContent);
                  leftSizes[l].fileDescriptors.removeAt(i);
                  rightSizes[r].fileDescriptors.removeAt(j);
                  break;
               }
            }
         }

         for (auto i = leftSizes.at(l).fileDescriptors.count(); i--; )
         {
            for (auto j = rightSizes.at(r).fileDescriptors.count(); j--; )
            {
               auto leftFileDescriptor = leftSizes.at(l).fileDescriptors.at(i);
               auto rightFileDescriptor = rightSizes.at(r).fileDescriptors.at(j);

               if (leftFileDescriptor.relFilePath != rightFileDescriptor.relFilePath && compareByContent(leftFileDescriptor.filePath, rightFileDescriptor.filePath) == 0)
               {
                  addNode(model->rootNode(), leftFileDescriptor.relFilePath, 0, rightFileDescriptor.relFilePath, leftFileDescriptor, rightFileDescriptor, (int)Status::EqualContent);
                  leftSizes[l].fileDescriptors.removeAt(i);
                  rightSizes[r].fileDescriptors.removeAt(j);
                  break;
               }
            }
         }
      }
   }

   // Match by file name

   leftList.clear();
   rightList.clear();

   for (auto&& sizeGroup : leftSizes)
   {
      leftList.append(sizeGroup.fileDescriptors);
   }

   for (auto&& sizeGroup : rightSizes)
   {
      rightList.append(sizeGroup.fileDescriptors);
   }

   std::sort(leftList.begin(), leftList.end(), greaterByPath);
   std::sort(rightList.begin(), rightList.end(), greaterByPath);

   l = leftList.count();
   r = rightList.count();

   while (true)
   {
      while (r && (l == 0 || rightList.at(r - 1).relFilePath < leftList.at(l - 1).relFilePath))
      {
         r--;
      }

      if (l == 0)
      {
         break;
      }

      l--;

      if (r && leftList.at(l).relFilePath == rightList.at(r - 1).relFilePath)
      {
         r--;

         addNode(model->rootNode(), leftList.at(l).relFilePath, 0, rightList.at(r).relFilePath, leftList.at(l), rightList.at(r), (int)Status::EqualName);
         leftList.removeAt(l);
         rightList.removeAt(r);
      }
   }

   // Add single files

   for (auto&& fileDescriptor : leftList)
   {
      addNode(model->rootNode(), fileDescriptor.relFilePath, 0, QStringList(), fileDescriptor, {}, (int)Status::Single);
   }

   for (auto&& fileDescriptor : rightList)
   {
      addNode(model->rootNode(), fileDescriptor.relFilePath, 0, fileDescriptor.relFilePath, {}, fileDescriptor, (int)Status::Single);
   }

   return model;
}
