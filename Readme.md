DirSync
=======

- Show the differences between two different directories in a tree view.
  DirSync can detect moved files.

- Synchronize two different directories.
