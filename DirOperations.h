#pragma once

#include <QDateTimeEx>
#include <QStringList>
#include <tuple>

class FileOperations
{
public:
   QStringList deletes;
   QList<std::tuple<QString, QString>> moves;
   QStringList copies;
   QList<std::tuple<QString, QDateTimeEx>> timeChanges;
};

class DirOperations : public QSharedData
{
public:
   DirOperations(const QString& leftFolder_, const QString& rightFolder_)
   {
      leftFolder = leftFolder_;
      rightFolder = rightFolder_;
   }

   QString leftFolder;
   FileOperations leftFileOperations;
   QString rightFolder;
   FileOperations rightFileOperations;
};
