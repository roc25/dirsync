#include "MainWindow.h"

#include <QApplication>
#include <QLocaleEx>
#include <BlueGradientProxyStyle>

int main(int argc, char *argv[])
{
   QLocaleEx locale;

   locale.setDateTimeFormat("dd.MM.yyyy hh:mm:ss");

   setDefaultLocale(locale);

   QApplication a(argc, argv);

   QApplication::setStyle(new BlueGradientProxyStyle(QApplication::style()));

   MainWindow w;
   w.show();
   return a.exec();
}
