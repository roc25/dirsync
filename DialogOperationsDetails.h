#ifndef DIALOGOPERATIONSDETAILS_H
#define DIALOGOPERATIONSDETAILS_H

#include <QDialog>
#include "DirOperations.h"

namespace Ui {
class DialogOperationsDetails;
}

class DialogOperationsDetails : public QDialog
{
   Q_OBJECT

public:
   explicit DialogOperationsDetails(QWidget *parent = nullptr);
   ~DialogOperationsDetails();

   void setFileOperations(const DirOperations& dirOperations);

   void setEmptyDirectories(const QString& leftRootPath, const QList<QStringList>& leftEmptyFolderList, const QString& rightRootPath, const QList<QStringList>& rightEmptyFolderList);

private slots:
   void on_pushButtonOk_clicked();

   void on_pushButtonAbort_clicked();

private:
   Ui::DialogOperationsDetails *ui;
};

#endif // DIALOGOPERATIONSDETAILS_H
