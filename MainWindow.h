#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <SmartPointer>
#include "DirSync.h"
#include "DirOperations.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButtonCompare_clicked();

    void on_toolButtonLeftFileSelector_clicked();

    void on_toolButtonRightFileSelector_clicked();

    void on_actionQuit_triggered();

    void on_pushButtonSwapPaths_clicked();

    void on_toolButtonShowEqual_toggled(bool checked);

    void on_pushButtonSynchronize_clicked();

    void on_toolButtonDirection_clicked();

    void on_treeViewEx_clicked(const QModelIndex &index);

    void on_treeViewEx_doubleClicked(const QModelIndex &index);

    void on_lineEditLeftFilePath_textChanged(const QString &arg1);

    void on_lineEditRightFilePath_textChanged(const QString &arg1);

private:
    void createFileLists();
    void buildTree();
    bp<DirOperations> getDirOperations() const;
    void visualizeTreeDirection();
    void getLastSyncTime();
    void setLastSyncTime();

private:
    Ui::MainWindow *ui;

    QList<FileDescriptor> _leftFileList;
    QList<QStringList> _leftEmptyFolderList;
    QList<FileDescriptor> _rightFileList;
    QList<QStringList> _rightEmptyFolderList;
    QDateTimeEx _lastSynchronized;
};
#endif // MAINWINDOW_H
