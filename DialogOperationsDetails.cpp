#include "DialogOperationsDetails.h"
#include "ui_DialogOperationsDetails.h"
#include <HtStandardModel>
#include <QDir>
#include <QtGuiVariantTypes.h>

static void addFileOperations(HtNode* parent, const FileOperations& fileOperations)
{
   auto deleteNode = parent->appendChild();

   deleteNode->setAttribute(0, "deletes");
   deleteNode->setDefaultAttribute(-1, QColor(255, 230, 205), Qt::BackgroundColorRole);

   for (auto&& relFileName : fileOperations.deletes)
   {
      auto node = deleteNode;

      for (auto&& filePart : relFileName.split(L'/'))
      {
         node = node->binaryFindOrInsert(0, filePart);
      }
   }

   auto moveNode = parent->appendChild();

   moveNode->setAttribute(0, "moves");
   moveNode->setDefaultAttribute(-1, QColor(230, 255, 230), Qt::BackgroundColorRole);

   for (auto&& moveInfo : fileOperations.moves)
   {
      auto node = moveNode;

      for (auto&& filePart : std::get<0>(moveInfo).split(L'/'))
      {
         node = node->binaryFindOrInsert(0, filePart);
      }

      node->setAttribute(1, std::get<1>(moveInfo));
   }

   auto copiesNode = parent->appendChild();

   copiesNode->setAttribute(0, "copies");
   copiesNode->setDefaultAttribute(-1, QColor(255, 255, 205), Qt::BackgroundColorRole);

   for (auto&& relFileName : fileOperations.copies)
   {
      auto node = copiesNode;

      for (auto&& filePart : relFileName.split(L'/'))
      {
         node = node->binaryFindOrInsert(0, filePart);
      }
   }

   auto timeChangeNode = parent->appendChild();

   timeChangeNode->setAttribute(0, "time changes");
   timeChangeNode->setDefaultAttribute(-1, QColor(230, 230, 255), Qt::BackgroundColorRole);

   for (auto&& timeChangeInfo : fileOperations.timeChanges)
   {
      auto node = timeChangeNode;

      for (auto&& filePart : std::get<0>(timeChangeInfo).split(L'/'))
      {
         node = node->binaryFindOrInsert(0, filePart);
      }

      node->setAttribute(1, std::get<1>(timeChangeInfo));
   }
}

static void addEmptyFolder(HtNode* node, const QList<QStringList>& emptyFolderList)
{
   for (auto&& emptyFolder : emptyFolderList)
   {
      auto folderNode = node->appendChild();

      folderNode->setAttribute(0, emptyFolder.join("/"));
   }
}

DialogOperationsDetails::DialogOperationsDetails(QWidget *parent) :
   QDialog(parent),
   ui(new Ui::DialogOperationsDetails)
{
   ui->setupUi(this);
}

DialogOperationsDetails::~DialogOperationsDetails()
{
   delete ui;
}

void DialogOperationsDetails::setFileOperations(const DirOperations& dirOperations)
{
   setWindowTitle(tr("Confirm file operations"));

   auto model = new HtStandardModel;

   model->setHeader(0, "");
   model->setHeader(1, "");

   auto leftChangesNode = model->rootNode()->appendChild();

   leftChangesNode->setAttribute(0, dirOperations.leftFolder);

   addFileOperations(leftChangesNode, dirOperations.leftFileOperations);

   auto rightChangesNode = model->rootNode()->appendChild();

   rightChangesNode->setAttribute(0, dirOperations.rightFolder);

   addFileOperations(rightChangesNode, dirOperations.rightFileOperations);

   ui->treeViewFileOperations->setModel(model);
   ui->treeViewFileOperations->setColumnWidth(0, 500);
   ui->treeViewFileOperations->setFirstColumnSpanned(0, QModelIndex(), true);
   ui->treeViewFileOperations->setFirstColumnSpanned(1, QModelIndex(), true);
   ui->treeViewFileOperations->expandAll();
}

void DialogOperationsDetails::setEmptyDirectories(const QString& leftRootPath, const QList<QStringList>& leftEmptyFolderList, const QString& rightRootPath, const QList<QStringList>& rightEmptyFolderList)
{
   setWindowTitle(tr("Empty folders to remove"));

   auto model = new HtStandardModel;

   model->setHeader(0, "");

   auto leftNode = model->rootNode()->appendChild();

   leftNode->setAttribute(0, leftRootPath);

   addEmptyFolder(leftNode, leftEmptyFolderList);

   auto rightNode = model->rootNode()->appendChild();

   rightNode->setAttribute(0, rightRootPath);

   addEmptyFolder(rightNode, rightEmptyFolderList);

   ui->treeViewFileOperations->setModel(model);
   ui->treeViewFileOperations->expandAll();
}

void DialogOperationsDetails::on_pushButtonOk_clicked()
{
    accept();
}

void DialogOperationsDetails::on_pushButtonAbort_clicked()
{
    reject();
}
