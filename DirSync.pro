QTXDIR = $$_PRO_FILE_PWD_/../QtExtensions

QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    DialogOperationsDetails.cpp \
    DirSync.cpp \
    MainWindow.cpp \
    main.cpp

HEADERS += \
    DialogOperationsDetails.h \
    DirOperations.h \
    DirSync.h \
    MainWindow.h

FORMS += \
    DialogOperationsDetails.ui \
    MainWindow.ui

TRANSLATIONS += \
    DirSync_de_DE.ts

unix: QMAKE_RPATHDIR += ${ORIGIN}

CONFIG(release, debug|release): LIBS += -L$$quote($$system_path($$QTXDIR/x64/Release))
else:CONFIG(debug, debug|release): LIBS += -L$$quote($$system_path($$QTXDIR/x64/Debug))

LIBS += -lQtCoreEx

INCLUDEPATH += $$quote($$system_path($$QTXDIR/include/QtCoreEx))
DEPENDPATH += $$quote($$system_path($$QTXDIR/include/QtCoreEx))

LIBS += -lQtGuiEx

INCLUDEPATH += $$quote($$system_path($$QTXDIR/include/QtGuiEx))
DEPENDPATH += $$quote($$system_path($$QTXDIR/include/QtGuiEx))

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
