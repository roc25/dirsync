#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <QFileDialog>
#include <QSettingsFile>
#include <QDesktopServices>
#include <QMessageBox>
#include <QtListExtensions>
#include <OverrideCursor>
#include <QtGuiVariantTypes.h>
#include "DialogOperationsDetails.h"
#ifdef Q_OS_WIN
#include <sys/utime.h>
#else
#include <utime.h>
#endif

static QString setModificationTime(const QString& filePath, const QDateTimeEx& lastModificationTime)
{
#ifdef Q_OS_WIN
   _utimbuf fileTime;
   wchar_t  *wszFileName = new wchar_t[filePath.length() + 1];

   filePath.toWCharArray(wszFileName);
   wszFileName[filePath.length()] = 0;

   fileTime.actime = QDateTime::currentDateTime().toSecsSinceEpoch();
   fileTime.modtime = lastModificationTime.toQDateTime().toSecsSinceEpoch();

   if (_wutime(wszFileName, &fileTime) != 0)
   {
      return QString::fromUtf8(strerror(errno));
   }

   delete[] wszFileName;
#else
    utimbuf fileTime;

    fileTime.actime = QDateTime::currentDateTime().toSecsSinceEpoch();
    fileTime.modtime = lastModificationTime.toQDateTime().toSecsSinceEpoch();

    if (utime(filePath.toLocal8Bit().data(), &fileTime) != 0)
    {
       return QString::fromUtf8(strerror(errno));
    }
#endif
    return QString();
}

static void getFileOperations(const HtNode* parent, DirOperations& dirOperations)
{
   for (auto&& child : parent->children())
   {
      getFileOperations(child, dirOperations);

      auto leftRelPath = child->attribute(2, Qt::UserRole).toString();
      auto rightRelPath = child->attribute(4, Qt::UserRole).toString();

      if (leftRelPath.isEmpty() && rightRelPath.isEmpty())
         continue;

      auto operation = child->attribute(3).toString();
      if (operation == "=>")
      {
         if (leftRelPath != rightRelPath)
         {
            // Move
            dirOperations.rightFileOperations.moves.append(std::make_tuple(leftRelPath, rightRelPath));
         }

         if (child->attribute(2).toDateTimeEx() != child->attribute(4).toDateTimeEx())
         {
            // Change modification time
            dirOperations.rightFileOperations.timeChanges.append(std::make_tuple(leftRelPath, child->attribute(2).toDateTimeEx()));
         }
      }
      else if (operation == "<=")
      {
         if (rightRelPath != leftRelPath)
         {
            // Move
            dirOperations.leftFileOperations.moves.append(std::make_tuple(rightRelPath, leftRelPath));
         }

         if (child->attribute(4).toDateTimeEx() != child->attribute(2).toDateTimeEx())
         {
            // Change modification time
            dirOperations.leftFileOperations.timeChanges.append(std::make_tuple(rightRelPath, child->attribute(4).toDateTimeEx()));
         }
      }
      else if (operation == "->")
      {
         if (!rightRelPath.isEmpty())
         {
            // Delete
            dirOperations.rightFileOperations.deletes.append(rightRelPath);
         }

         if (!leftRelPath.isEmpty())
         {
            // Copy
            dirOperations.rightFileOperations.copies.append(leftRelPath);

            // Change modification time
            dirOperations.rightFileOperations.timeChanges.append(std::make_tuple(leftRelPath, child->attribute(2).toDateTimeEx()));
         }
      }
      else if (operation == "<-")
      {
         if (!leftRelPath.isEmpty())
         {
            // Delete
            dirOperations.leftFileOperations.deletes.append(leftRelPath);
         }

         if (!rightRelPath.isEmpty())
         {
            // Copy
            dirOperations.leftFileOperations.copies.append(rightRelPath);

            // Change modification time
            dirOperations.leftFileOperations.timeChanges.append(std::make_tuple(rightRelPath, child->attribute(4).toDateTimeEx()));
         }
      }
   }
}

template <typename T>
bool filterTree(QTreeView* tree, const HtItemModel* model, const HtNode* node, const T& filter, bool parentIsVisible = false)
{
   bool match = filter(node);
   auto childMatch = false;

   for (auto&& child : node->children())
   {
      if (filterTree(tree, model, child, filter, parentIsVisible || match))
         childMatch = true;
   }

   tree->setRowHidden(node->parent()->indexOf(node), model->indexFromNode(node->parent()), !parentIsVisible && !match && !childMatch);
   if (childMatch)
      tree->expand(model->indexFromItem(node));
   else
      tree->collapse(model->indexFromItem(node));

   return match || childMatch;
}

static void setFileChangedWarning(HtNode* node, int index, QStyle* style)
{
   node->setAttribute(index, style->standardIcon(QStyle::SP_MessageBoxWarning), Qt::DecorationRole);
   node->setAttribute(index, "The file changed after last synchronization!", Qt::ToolTipRole);
}

static const QColor totalLossColor = QColor(255, 64, 64);
static const QColor partialLossColor = QColor(255, 128, 64);
static const QColor minorLossColor = QColor(255, 255, 64);

static void setSyncDirection(HtNode* node, int direction, const QDateTimeEx& lastSyncTime, QStyle* style)
{
   if (node->hasChildren())
   {
      for (auto&& child : node->children())
      {
         setSyncDirection(child, direction, lastSyncTime, style);
      }
      return;
   }

   auto status = node->attribute(3, Qt::UserRole).toInt();
   bool equalNames = status & (int)Status::EqualName;
   bool equalTimes = status & (int)Status::EqualTime;
   bool equalContents = status & (int)Status::EqualContent;

   node->setAttribute(0, Variant::null, Qt::BackgroundColorRole);
   node->setAttribute(0, Variant::null, Qt::DecorationRole);
   node->setAttribute(0, Variant::null, Qt::ToolTipRole);
   node->setAttribute(6, Variant::null, Qt::BackgroundColorRole);
   node->setAttribute(6, Variant::null, Qt::DecorationRole);
   node->setAttribute(6, Variant::null, Qt::ToolTipRole);

   if (equalNames && equalTimes && equalContents)
   {
      node->setAttribute(3, "==");
   }
   else if (status == (int)Status::Single)
   {
      auto defaultDirection = node->attribute(1).isNull() ? -1 : 1;

      if (direction == 0)
         direction = defaultDirection;

      node->setAttribute(3, direction < 0 ? "<-" : "->");
      if (direction != defaultDirection)
      {
         node->setAttribute(direction < 0 ? 0 : 6, totalLossColor, Qt::BackgroundColorRole);
         if (lastSyncTime.isValid() && node->attribute(direction < 0 ? 2 : 4).toDateTimeEx() > lastSyncTime)
         {
            setFileChangedWarning(node, direction < 0 ? 0 : 6, style);
         }
      }
   }
   else if (node->attribute(2).toDateTimeEx() < node->attribute(4).toDateTimeEx())
   {
      if (direction > 0)
      {
         node->setAttribute(3, equalContents ? "=>" : "->");
         node->setAttribute(6, equalContents ? minorLossColor : partialLossColor, Qt::BackgroundColorRole);
      }
      else
      {
         node->setAttribute(3, equalContents ? "<=" : "<-");
         if (lastSyncTime.isValid() && node->attribute(2).toDateTimeEx() > lastSyncTime)
         {
            node->setAttribute(0, equalContents ? minorLossColor : partialLossColor, Qt::BackgroundColorRole);
            setFileChangedWarning(node, 0, style);
         }
      }
   }
   else if (node->attribute(2).toDateTimeEx() > node->attribute(4).toDateTimeEx())
   {
      if (direction < 0)
      {
         node->setAttribute(3, equalContents ? "<=" : "<-");
         node->setAttribute(0, equalContents ? minorLossColor : partialLossColor, Qt::BackgroundColorRole);
      }
      else
      {
         node->setAttribute(3, equalContents ? "=>" : "->");
         if (lastSyncTime.isValid() && node->attribute(4).toDateTimeEx() > lastSyncTime)
         {
            node->setAttribute(6, equalContents ? minorLossColor : partialLossColor, Qt::BackgroundColorRole);
            setFileChangedWarning(node, 6, style);
         }
      }
   }
   else
   {
      if (direction > 0)
      {
         node->setAttribute(3, equalContents ? "=>" : "->");
         if (lastSyncTime.isValid() && node->attribute(4).toDateTimeEx() > lastSyncTime)
         {
            node->setAttribute(6, equalContents ? minorLossColor : partialLossColor, Qt::BackgroundColorRole);
            setFileChangedWarning(node, 6, style);
         }
      }
      else if (direction < 0)
      {
         node->setAttribute(3, equalContents ? "<=" : "<-");
         if (lastSyncTime.isValid() && node->attribute(2).toDateTimeEx() > lastSyncTime)
         {
            node->setAttribute(0, equalContents ? minorLossColor : partialLossColor, Qt::BackgroundColorRole);
            setFileChangedWarning(node, 0, style);
         }
      }
      else
      {
         node->setAttribute(3, equalContents ? "<=>" : "<->");
      }
   }
}

static void updateFolderAction(HtNode* node)
{
   if (!node->hasChildren())
      return;

   QString syncMode;

   for (auto&& child : node->children())
   {
      updateFolderAction(child);

      auto childMode = child->attribute(3).toString();

      if (syncMode.isEmpty())
      {
         syncMode = childMode;
      }
      else if (childMode != syncMode)
      {
         syncMode.clear();
         break;
      }
   }

   node->setAttribute(3, syncMode);
}

static void updateParentFolderAction(HtNode* node)
{
   auto parent = node->parent();

   if (parent->parent() == nullptr)
      return;

   QString syncMode;

   for (auto&& child : parent->children())
   {
      auto childMode = child->attribute(3).toString();

      if (syncMode.isEmpty())
      {
         syncMode = childMode;
      }
      else if (childMode != syncMode)
      {
         syncMode.clear();
         break;
      }
   }

   parent->setAttribute(3, syncMode);

   updateParentFolderAction(parent);
}

static void adjustMoveOperations(QList<std::tuple<QString, QString>>& moves)
{
   auto begin = 0;
   while (begin < moves.size())
   {
      auto predecessorIndex = begin;

      begin++;

      for (int i = predecessorIndex; ++i < moves.size(); )
      {
         if (std::get<1>(moves.at(i)) == std::get<0>(moves.at(predecessorIndex)))
         {
            if (i < begin)
            {
               auto tmpFile = std::get<0>(moves.at(predecessorIndex));
               tmpFile.insert(tmpFile.lastIndexOf(L'/') + 1, "~!");

               moves.insert(predecessorIndex, std::make_tuple(tmpFile, std::get<0>(moves.at(predecessorIndex))));
               begin++;

               std::get<1>(moves[i]) = tmpFile;

               break;
            }

            moves.move(i, predecessorIndex);
            i = predecessorIndex;
            begin++;
         }
      }
   }
}

static QString executeFileOperations(const FileOperations& fileOperations, const QDir& sourceDir, const QDir& destDir)
{
   for (auto&& relFileName : fileOperations.deletes)
   {
      QFile file(destDir.filePath(relFileName));

      if (!file.remove())
      {
         return file.errorString();
      }
   }

   for (auto&& moveInfo : fileOperations.moves)
   {
      auto relDestFileName = std::get<0>(moveInfo);
      auto relDestFolder = QFileInfo(relDestFileName).path();
      if (!relDestFolder.isEmpty())
      {
         if (!destDir.mkpath(relDestFolder))
         {
            return QMainWindow::tr("Path could not be created: %1").arg(destDir.filePath(relDestFolder));
         }
      }

      QFile file(destDir.filePath(std::get<1>(moveInfo)));

      if (!file.rename(destDir.filePath(relDestFileName)))
      {
         return file.errorString();
      }
   }

   for (auto&& relFileName : fileOperations.copies)
   {
      auto relDestFolder = QFileInfo(relFileName).path();
      if (!relDestFolder.isEmpty())
      {
         if (!destDir.mkpath(relDestFolder))
         {
            return QMainWindow::tr("Path could not be created: %1").arg(destDir.filePath(relDestFolder));
         }
      }

      QFile file(sourceDir.filePath(relFileName));

      if (!file.copy(destDir.filePath(relFileName)))
      {
         return file.errorString();
      }
   }

   for (auto&& timeChangeInfo : fileOperations.timeChanges)
   {
      auto errorMsg = setModificationTime(destDir.filePath(std::get<0>(timeChangeInfo)), std::get<1>(timeChangeInfo));

      if (!errorMsg.isEmpty())
      {
         return errorMsg;
      }
   }

   return {};
}

MainWindow::MainWindow(QWidget *parent)
   : QMainWindow(parent)
   , ui(new Ui::MainWindow)
{
   ui->setupUi(this);

   QSettingsFile settings("roxoft/DirSync", QSettingsFile::SM_VariantSerialize);

   auto windowSettigs = settings.value("Window").toByteArray();

   if (!windowSettigs.isEmpty())
      this->restoreState(windowSettigs);

   auto geometry = settings.value("Geometry").toByteArray();

   if (!geometry.isEmpty())
      this->restoreGeometry(geometry);

   ui->lineEditLeftFilePath->setText(settings.value("LeftPath").toString());
   ui->lineEditRightFilePath->setText(settings.value("RightPath").toString());
   ui->toolButtonShowEqual->setChecked(settings.value("ShowEquals").toBool());
}

MainWindow::~MainWindow()
{
   QSettingsFile settings("roxoft/DirSync", QSettingsFile::SM_VariantSerialize);

   settings.setValue("Geometry", this->saveGeometry());
   settings.setValue("Window", this->saveState());

   if (ui->treeViewEx->model())
      settings.setValue("Tree", ui->treeViewEx->header()->saveState());
   settings.setValue("LeftPath", ui->lineEditLeftFilePath->text());
   settings.setValue("RightPath", ui->lineEditRightFilePath->text());
   settings.setValue("ShowEquals", ui->toolButtonShowEqual->isChecked());

   delete ui;
}

void MainWindow::on_pushButtonCompare_clicked()
{
   OverrideCursor waitCursor;

   createFileLists();

   getLastSyncTime();

   buildTree();
}

void MainWindow::on_toolButtonLeftFileSelector_clicked()
{
   auto filePath = QFileDialog::getExistingDirectory(this, tr("Folder to sync"), ui->lineEditLeftFilePath->text());

   if (!filePath.isEmpty())
      ui->lineEditLeftFilePath->setText(filePath);
}

void MainWindow::on_toolButtonRightFileSelector_clicked()
{
   auto filePath = QFileDialog::getExistingDirectory(this, tr("Folder to sync"), ui->lineEditRightFilePath->text());

   if (!filePath.isEmpty())
      ui->lineEditRightFilePath->setText(filePath);
}

void MainWindow::on_actionQuit_triggered()
{
   this->close();
}

void MainWindow::on_pushButtonSwapPaths_clicked()
{
   OverrideCursor waitCursor;

   auto filePath = ui->lineEditLeftFilePath->text();
   ui->lineEditLeftFilePath->setText(ui->lineEditRightFilePath->text());
   ui->lineEditRightFilePath->setText(filePath);
   std::swap(_leftFileList, _rightFileList);
   buildTree();
}

void MainWindow::on_toolButtonShowEqual_toggled(bool checked)
{
   OverrideCursor waitCursor;

   auto model = dynamic_cast<const HtItemModel*>(ui->treeViewEx->model());

   if (model == nullptr)
   {
      return;
   }

   if (checked)
   {
      for (auto&& child : model->rootNode()->children())
         filterTree(ui->treeViewEx, model, child, [](const HtNode*){ return true; });
   }
   else
   {
      for (auto&& child : model->rootNode()->children())
         filterTree(ui->treeViewEx, model, child, [](const HtNode* node){ return node->attribute(3).toString() != "==" && !node->attribute(3).toString().isEmpty(); });
   }
}

void MainWindow::on_pushButtonSynchronize_clicked()
{
   auto dirOperations = getDirOperations();

   DialogOperationsDetails detailsDialog(this);

   detailsDialog.setFileOperations(*dirOperations);

   if (detailsDialog.exec() == QDialog::Rejected)
   {
      return;
   }

   OverrideCursor waitCursor;

   QDir leftRootDir(dirOperations->leftFolder);
   QDir rightRootDir(dirOperations->rightFolder);
   QString errorMsg;

   errorMsg = executeFileOperations(dirOperations->leftFileOperations, rightRootDir, leftRootDir);

   if (errorMsg.isEmpty())
   {
      errorMsg = executeFileOperations(dirOperations->rightFileOperations, leftRootDir, rightRootDir);
   }

   setLastSyncTime();

   createFileLists();

   if (!_leftEmptyFolderList.empty() || !_rightEmptyFolderList.empty())
   {
      DialogOperationsDetails emptyFolderDetails(this);

      emptyFolderDetails.setEmptyDirectories(dirOperations->leftFolder, _leftEmptyFolderList, dirOperations->rightFolder, _rightEmptyFolderList);

      NormalCursor normalCursor;

      auto removeEmptyFolderAccepted = emptyFolderDetails.exec() == QDialog::Accepted;

      normalCursor.restore();

      if (removeEmptyFolderAccepted)
      {
         for (auto&& leftFolder : _leftEmptyFolderList)
         {
            if (!leftRootDir.rmdir(leftFolder.join("/")))
            {
               QMessageBox::warning(this, tr("Remove empty directories"), tr("Folder \"%1\" could not be removed").arg(leftRootDir.filePath(leftFolder.join("/"))));
            }
         }

         for (auto&& rightFolder : _rightEmptyFolderList)
         {
            if (!rightRootDir.rmdir(rightFolder.join("/")))
            {
               QMessageBox::warning(this, tr("Remove empty directories"), tr("Folder \"%1\" could not be removed").arg(rightRootDir.filePath(rightFolder.join("/"))));
            }
         }
      }
   }

   buildTree();

   waitCursor.restore();

   if (!errorMsg.isEmpty())
   {
      QMessageBox::critical(this, this->windowTitle(), errorMsg);
   }
}

void MainWindow::on_toolButtonDirection_clicked()
{
   OverrideCursor waitCursor;

   if (ui->toolButtonDirection->text() == "<-->")
   {
      ui->toolButtonDirection->setText("-->");
   }
   else if (ui->toolButtonDirection->text() == "-->")
   {
      ui->toolButtonDirection->setText("<--");
   }
   else
   {
      ui->toolButtonDirection->setText("<-->");
   }

   visualizeTreeDirection();
}

void MainWindow::on_treeViewEx_clicked(const QModelIndex &index)
{
   if (index.column() == 3)
   {
      auto node = dynamic_cast<HtItemModel*>(ui->treeViewEx->model())->nodeFromIndex(index);
      auto comparator = node->attribute(3).toString();

      if (comparator == "<->" || comparator == "<=>")
      {
         setSyncDirection(node, 1, _lastSynchronized, style());
      }
      else if (comparator == "->" || comparator == "=>")
      {
         setSyncDirection(node, -1, _lastSynchronized, style());
      }
      else if (comparator == "<-" || comparator == "<=")
      {
         node->setAttribute(3, comparator == "<-" ? "<->" : "<=>");

         node->setAttribute(0, Variant::null, Qt::BackgroundColorRole);
         node->setAttribute(0, Variant::null, Qt::DecorationRole);
         node->setAttribute(0, Variant::null, Qt::ToolTipRole);
         node->setAttribute(6, Variant::null, Qt::BackgroundColorRole);
         node->setAttribute(6, Variant::null, Qt::DecorationRole);
         node->setAttribute(6, Variant::null, Qt::ToolTipRole);
      }

      updateParentFolderAction(node);
   }
}

void MainWindow::on_treeViewEx_doubleClicked(const QModelIndex &index)
{
   QString filePath;

   if (index.column() == 0)
   {
      filePath = index.siblingAtColumn(1).data(Qt::UserRole).toString();
   }
   else if (index.column() == 6)
   {
      filePath = index.siblingAtColumn(5).data(Qt::UserRole).toString();
   }

   if (!filePath.isEmpty())
      QDesktopServices::openUrl(QUrl(QString("file://%1").arg(filePath)));
}

void MainWindow::on_lineEditLeftFilePath_textChanged(const QString &arg1)
{
    auto model = ui->treeViewEx->model();

    if (model)
    {
       ui->treeViewEx->setModel(nullptr);
       delete model;
    }
}

void MainWindow::on_lineEditRightFilePath_textChanged(const QString &arg1)
{
   auto model = ui->treeViewEx->model();

   if (model)
   {
      ui->treeViewEx->setModel(nullptr);
      delete model;
   }
}

//
// Helper methods
//

void MainWindow::createFileLists()
{
   _leftFileList.clear();
   _leftEmptyFolderList.clear();
   _rightFileList.clear();
   _rightEmptyFolderList.clear();

   QString leftFolderPath = ui->lineEditLeftFilePath->text();
   QString rightFolderPath = ui->lineEditRightFilePath->text();

   parse(leftFolderPath, _leftFileList, _leftEmptyFolderList);
   parse(rightFolderPath, _rightFileList, _rightEmptyFolderList);
}

void MainWindow::buildTree()
{
   auto model = createSyncModel(_leftFileList, _rightFileList);

   model->setParent(this);

   model->setHeader(0, tr("Name"));
   model->setHeader(1, tr("Size"));
   model->setHeader(2, tr("Modified"));
   model->setHeader(3, tr("Action"));
   model->setHeader(4, tr("Modified"));
   model->setHeader(5, tr("Size"));
   model->setHeader(6, tr("Name"));

   model->rootNode()->setDefaultFlags(3, Qt::AlignHCenter, Qt::TextAlignmentRole);

   QSettingsFile settings("roxoft/DirSync", QSettingsFile::SM_VariantSerialize);

   auto oldModel = ui->treeViewEx->model();

   if (oldModel)
   {
      settings.setValue("Tree", ui->treeViewEx->header()->saveState());
   }

   ui->treeViewEx->setModel(model);

   if (oldModel)
   {
      ui->treeViewEx->header()->restoreState(settings.value("Tree").toByteArray());
      delete oldModel;
   }
   else
   {
      ui->treeViewEx->header()->setSectionResizeMode(0, QHeaderView::Stretch);
      ui->treeViewEx->header()->setSectionResizeMode(1, QHeaderView::Interactive);
      ui->treeViewEx->header()->setSectionResizeMode(2, QHeaderView::Interactive);
      ui->treeViewEx->header()->setSectionResizeMode(3, QHeaderView::Interactive);
      ui->treeViewEx->header()->setSectionResizeMode(4, QHeaderView::Interactive);
      ui->treeViewEx->header()->setSectionResizeMode(5, QHeaderView::Interactive);
      ui->treeViewEx->header()->setSectionResizeMode(0, QHeaderView::Stretch);
      ui->treeViewEx->header()->resizeSection(1, 80);
      ui->treeViewEx->header()->resizeSection(2, 130);
      ui->treeViewEx->header()->resizeSection(3, 40);
      ui->treeViewEx->header()->resizeSection(4, 130);
      ui->treeViewEx->header()->resizeSection(5, 80);
      ui->treeViewEx->header()->setStretchLastSection(true);
   }

   ui->treeViewEx->expandAll();

   visualizeTreeDirection();

   if (!ui->toolButtonShowEqual->isChecked())
   {
      on_toolButtonShowEqual_toggled(false);
   }
}

bp<DirOperations> MainWindow::getDirOperations() const
{
   OverrideCursor waitCursor;

   auto dirOperations = bp_new<DirOperations>(ui->lineEditLeftFilePath->text(), ui->lineEditRightFilePath->text());

   getFileOperations(dynamic_cast<const HtItemModel*>(ui->treeViewEx->model())->rootNode(), *dirOperations);

   adjustMoveOperations(dirOperations->leftFileOperations.moves);
   adjustMoveOperations(dirOperations->rightFileOperations.moves);

   return dirOperations;
}

void MainWindow::visualizeTreeDirection()
{
   auto rootNode = dynamic_cast<HtItemModel*>(ui->treeViewEx->model())->rootNode();
   if (rootNode->hasChildren())
   {
      setSyncDirection(
               rootNode,
               ui->toolButtonDirection->text() == "<--" ? -1 : ui->toolButtonDirection->text() == "-->" ? 1 : 0
               , _lastSynchronized,
               style());
      for (auto&& child : rootNode->children())
      {
         updateFolderAction(child);
      }
   }
}

void MainWindow::getLastSyncTime()
{
   // Get last synchronized time for the selected folders

   QSettingsFile settings("roxoft/DirSync", QSettingsFile::SM_VariantSerialize);

   auto firstDir = ui->lineEditLeftFilePath->text();
   auto secondDir = ui->lineEditRightFilePath->text();

   if (secondDir < firstDir)
   {
      qSwap(firstDir, secondDir);
   }

   _lastSynchronized = QDateTimeEx();

   auto lastSyncList = settings.currentSetting()->const_settings("Synchronized");

   for (auto&& lastSyncSettings : lastSyncList)
   {
      if (lastSyncSettings->value("FirstDir").toString() == firstDir && lastSyncSettings->value("SecondDir").toString() == secondDir)
      {
         _lastSynchronized = lastSyncSettings->value("Timestamp").toDateTimeEx();
         break;
      }
   }

   if (_lastSynchronized.isValid())
      ui->labelLastSyncTime->setText(tr("Last synchronized: %1").arg(_lastSynchronized.toLocalTime().toString("dd.MM.yyyy hh:mm")));
   else
      ui->labelLastSyncTime->setText(QString());
}

void MainWindow::setLastSyncTime()
{
   // Set the last synchronized time for the selected folders

   QSettingsFile settings("roxoft/DirSync", QSettingsFile::SM_VariantSerialize);

   auto firstDir = ui->lineEditLeftFilePath->text();
   auto secondDir = ui->lineEditRightFilePath->text();

   if (secondDir < firstDir)
   {
      qSwap(firstDir, secondDir);
   }

   _lastSynchronized = QDateTimeEx::currentDateTimeUtc();

   ui->labelLastSyncTime->setText(tr("Last synchronized: %1").arg(_lastSynchronized.toLocalTime().toString("dd.MM.yyyy hh:mm")));

   auto lastSyncList = settings.currentSetting()->settings("Synchronized");

   for (auto&& lastSyncSettings : lastSyncList)
   {
      if (lastSyncSettings->value("FirstDir").toString() == firstDir && lastSyncSettings->value("SecondDir").toString() == secondDir)
      {
         lastSyncSettings->setting("Timestamp")->setValue(_lastSynchronized);
         return;
      }
   }

   auto lastSyncSettings = settings.currentSetting()->addSetting("Synchronized");

   lastSyncSettings->setting("FirstDir")->setValue(firstDir);
   lastSyncSettings->setting("SecondDir")->setValue(secondDir);
   lastSyncSettings->setting("Timestamp")->setValue(_lastSynchronized);
}
