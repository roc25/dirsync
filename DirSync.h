#pragma once
#include <HtItemModel>
#include <QDateTimeEx>
#include <QDir>

enum class Status { Single = 0, EqualName = 1, EqualTime = 2, EqualContent = 4 };

class FileDescriptor
{
public:
   QString filePath;
   QStringList relFilePath;
   qint64 size = 0LL;
   QDateTimeEx lastModified;

   bool operator==(const FileDescriptor& other) const { return filePath == other.filePath; }
   bool operator!=(const FileDescriptor& other) const { return filePath != other.filePath; }

   bool operator<(const FileDescriptor& other) const { return filePath < other.filePath; }
   bool operator>(const FileDescriptor& other) const { return filePath > other.filePath; }
};

void parse(const QDir &rootDir, QList<FileDescriptor>& fileList, QList<QStringList> &emptyFolderList, const QStringList& folderPath = QStringList()); // Recursive function!

HtItemModel* createSyncModel(QList<FileDescriptor> leftList, QList<FileDescriptor> rightList);
